Guru Point 05
=============

Instructions
------------

To get **one** extra credit point, you must do the following:

1. **Fork** this repository on [Bitbucket].

2. **Clone** your fork to a local machine.

3. In your clone, **create** a feature [branch] using [git] called
   `$NETID-GuruPoint05`.

4. In this new branch, **modify** this `README.md` to include one cool or
   interesting thing about yourself.  Follow the convention below.

5. **Commit** and **push** the new branch to your [Bitbucket] fork.

6. Submit a [pull request] to the original [Guru Point 05] repository.

You will get credit when your [pull request] has been accepted.

**Note**: The instructor will only accept a [pull request] that has no
conflicts with the existing **master** branch.  This means you need to be sure
your [pull request] can apply cleanly.  If it does not, you will need to
respond to the comments in the [pull request] to get your [branch] into proper
shape.

[Bitbucket]:    https://bitbucket.org/
[git]:          https://git-scm.com/
[branch]:       https://www.atlassian.com/git/tutorials/using-branches/
[pull request]: https://www.atlassian.com/git/tutorials/making-a-pull-request/how-it-works
[Guru Point 05]:https://bitbucket.org/CSE-20189-SP16/gurupoint_05

Peoples
-------

### Peter Bui

Some people call me Dr. Bui, but not my **mom**.  She says I'm not a real
doctor because I work on **computers** and not **people**.

### Brent Marin

I have a county named after me near San Fransisco. Google it.

### John Westhoff

Hmmm... I have a scar under my **left eyebrow** from when I hit my head on 
the edge of a staircase when I was ~two, a scar on my **left shoulder**
from when I had surgery when I was ~five, a scar on my **right elbow**
from when I fell off a scooter when I was ~twelve, and a scar on my 
**left heel** from when I tried to do a cartwheel and I kicked my foot into
a coffee table.

### Xuanyi Li

I am right-handed but I have been training myself to write with my left hand 
since six grade. When a class is boring, I take my notes with my left hand.

### Aaron Crawfis

Despite the name similarity I have actually never eaten crawfish before....
probably because I'm from Michigan and we don't have those there.

### Kat Herring

When my sister and I were growing up, she was afraid of heights. Being the
loving older sister that I am, we decided to enroll her in "bravery camp"- a
training regime of my own (eight-year-old) design, which involved such tasks as
jumping out of trees and walking on balance beams thirty feet in the air. It
worked. She is going skydiving with me this summer.

### Cameron Smick

In my free time, I go climbing at the rock wall in Rockne. Soon, once I get
belay certified, I'll go on outdoor climbing trips with the Climbing Club on
campus.

### Pauline Blatt

My dorm flag football (B) team made it to the championship and got to play in
the stadium earlier this year, and I caught the winning touchdown! Woop woop!


### Chris Rho

I got my name from being born on Christmas, and my roomate and I are the only comp sci sophomores in our dorm, ST. ED's.

### Connor Higgins

I have more pets than siblings, and I have 7 siblings. I have 4 dogs, 3 rabbits, and an albino
guinea pig.

### Joey Curci

I am actually not an interesting person, I just like getting extra credit.

### Courtney Kelly

I hate the sound of people eating apples. If you are eating one in class, I
will know, and I will hate you for it.

### Daniel Kerrigan

Mary Sawyer, the subject of the nursery rhyme "Mary Had a Little Lamb", lived
in my hometown, Sterling, Massachusetts.

### John Johnson

I am the third John Johnson, but not John Johnson III because we all have
different middle names. No, my middle name is not John, Jonathan, or Johnny.

### James Farrington

I'm just here so I don't get fined.

### Jorge Diaz-Ortiz

I have switched majors 3 times at ND. Don't worry, I've always been an
engineer: ME -> EE -> CPEG.

### Will Badart

I don't think anyone calls me Dr. Bui. That's a true fact.

### Anthony Daegele

I was in Barcenlona when they won the 2015 Champions League Final. It was
pretty crazy.
